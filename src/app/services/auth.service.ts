import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICredentials } from '../models/ICredentials';
import { IUser } from '../models/IUser';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  login(credentials: ICredentials): Observable<{ user: IUser, token: string }> {
    return this.http.post<{ user: IUser, token: string }>('/login', credentials);
  }

  register(credentials: ICredentials): Observable<IUser> {
    return this.http.post<IUser>('/register', credentials);
  }
}
