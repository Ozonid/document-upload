import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { State } from '../store/reducers/auth.reducer';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  private isAuthenticated$: Observable<boolean>;

  constructor(
    private store: Store<{ auth: State }>,
    public router: Router
  ) {
    this.isAuthenticated$ = this.store.pipe(select((state) => state.auth.isAuthenticated));
  }

  canActivate(): Observable<boolean> {
    return this.isAuthenticated$.pipe(
      tap((isAuthenticated) => {
        if (!isAuthenticated) {
          this.router.navigate(['login']);
        }
      })
    );
  }
}
