import { Injectable } from '@angular/core';
import { IDocument } from '../models/IDocument';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DocumentsService {

  constructor(private http: HttpClient) { }

  getDocuments(): Observable<IDocument[]> {
    return this.http.get<IDocument[]>('/documents');
  }

  createDocuments(documents: string[]): Observable<IDocument[]> {
    return this.http.post<IDocument[]>('/documents', documents);
  }

  deleteDocuments(documentIds: number[]): Observable<IDocument[]> {
    return this.http.post<IDocument[]>('/documents/delete', documentIds);
  }

  categorizeDocuments(data: { documentIds: number[], categoryName: string}): Observable<IDocument[]> {
    return this.http.post<IDocument[]>('/documents/category', data);
  }
}
