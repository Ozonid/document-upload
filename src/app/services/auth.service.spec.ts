import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AuthService } from './auth.service';

describe('AuthService', () => {
  let httpTestingController: HttpTestingController;
  let service: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthService],
      imports: [HttpClientTestingModule]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(AuthService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#login', () => {
    it('should call /login with provided credentials', () => {
      const credentials = {
        email: 'test@email.com',
        password: 'password'
      };
      const mockLoginReponse = {
        user: {
          ...credentials,
          id: 1
        },
        token: 'token'
      };

      service.login(credentials)
        .subscribe((resp) => {
          expect(resp).toEqual(mockLoginReponse);
        });

      const req = httpTestingController.expectOne('/login');
      expect(req.request.method).toEqual('POST');
      expect(req.request.body).toEqual(credentials);
      req.flush(mockLoginReponse);
    });
  });

  describe('#register', () => {
    it('should call /register with provided credentials', () => {
      const credentials = {
        email: 'test@email.com',
        password: 'password'
      };
      const mockLoginReponse = {
        user: {
          ...credentials,
          id: 1
        }
      };

      service.register(credentials)
        .subscribe((resp) => {
          expect(resp).toEqual(mockLoginReponse);
        });

      const req = httpTestingController.expectOne('/register');
      expect(req.request.method).toEqual('POST');
      expect(req.request.body).toEqual(credentials);
      req.flush(mockLoginReponse);
    });
  });
});
