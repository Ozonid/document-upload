import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './public/login/login.component';
import { RegisterComponent } from './public/register/register.component';
import { DocumentsComponent } from './private/documents/documents.component';
import { AuthGuardService } from './services/auth-guard.service';


const publicRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
];

const privateRoutes: Routes = [
  { path: '', redirectTo: '/documents', pathMatch: 'full' },
  {
    path: '',
    canActivate: [AuthGuardService],
    children: [
      { path: 'documents', component: DocumentsComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot([...publicRoutes, ...privateRoutes])],
  exports: [RouterModule]
})
export class AppRoutingModule { }
