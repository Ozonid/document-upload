import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { login } from 'src/app/store/actions/auth.actions';
import { Store } from '@ngrx/store';
import { State } from 'src/app/store/reducers/auth.reducer';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public errorMessages$ = this.store.select((state) => state.auth.errorMessage);

  public loginForm = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.email
    ]),
    password: new FormControl('', [
      Validators.required
    ]),
  });

  constructor(
    private store: Store<{ auth: State }>
  ) { }

  public onSubmit(): void {
    this.store.dispatch(login(this.loginForm.value));
  }
}
