import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { register } from 'src/app/store/actions/auth.actions';
import { Store } from '@ngrx/store';
import { State } from 'src/app/store/reducers/auth.reducer';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  public errorMessages$ = this.store.select((state) => state.auth.errorMessage);

  public registerForm = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.email
    ]),
    password: new FormControl('', [
      Validators.required
    ]),
  });

  constructor(
    private store: Store<{ auth: State }>
  ) { }

  public onSubmit(): void {
    this.store.dispatch(register(this.registerForm.value));
  }
}
