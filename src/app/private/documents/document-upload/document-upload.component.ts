import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from 'src/app/store/reducers/documents.reducer';
import { FormControl, FormGroup } from '@angular/forms';
import { uploadDocuments } from 'src/app/store/actions/document.actions';
import * as _ from 'lodash';

@Component({
  selector: 'app-document-upload',
  templateUrl: './document-upload.component.html',
  styleUrls: ['./document-upload.component.scss']
})
export class DocumentUploadComponent {
  public uploadForm = new FormGroup({
    documents: new FormControl(''),
  });

  public newDocuments: string[] = [];
  constructor(
    private store: Store<{ documents: State }>
  ) { }

  onFileChange(event) {
    this.newDocuments = _.map(event.target.files, (d) => d.name);
  }

  onSubmit() {
    this.store.dispatch(uploadDocuments({ documents: this.newDocuments }));
    this.newDocuments = [];
  }

}
