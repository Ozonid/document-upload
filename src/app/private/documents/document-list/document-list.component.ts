import { Component, OnInit } from '@angular/core';
import { fetchDocuments, toggleDocumentSelection, toggleCategorySelection } from 'src/app/store/actions/document.actions';
import { Store } from '@ngrx/store';
import { State } from 'src/app/store/reducers/documents.reducer';
import * as _ from 'lodash';

@Component({
  selector: 'app-document-list',
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.scss']
})
export class DocumentListComponent implements OnInit {
  public sectionOpen = 'null';

  public categories$ = this.store.select((state) => _.groupBy(state.documents.documents, 'category'));
  public selectedDocumentIds$ = this.store.select((state) => state.documents.selectedDocumentIds);
  public selectedCategory$ = this.store.select((state) => state.documents.selectedCategory);

  constructor(
    private store: Store<{ documents: State }>
  ) { }

  ngOnInit() {
    this.store.dispatch(fetchDocuments());
  }

  toggleSelection(id: number) {
    this.store.dispatch(toggleDocumentSelection({ documentId: id }));
  }

  toggleSection(name: string) {
    this.store.dispatch(toggleCategorySelection({ categoryName: name }));
  }
}
