import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import * as _ from 'lodash';
import { Store } from '@ngrx/store';
import { State } from 'src/app/store/reducers/documents.reducer';
import { addDocumentsToCategory } from 'src/app/store/actions/document.actions';

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.scss']
})
export class ControlPanelComponent {
  public categoryForm = new FormGroup({
    name: new FormControl(''),
  });

  public selectedDocumentIds$ = this.store.select((state) => state.documents.selectedDocumentIds);

  constructor(
    private store: Store<{ documents: State }>
  ) { }

  onCategorySubmit() {
    this.store.dispatch(addDocumentsToCategory({
      categoryName: this.categoryForm.value.name
    }));

    this.categoryForm.reset();
  }
}
