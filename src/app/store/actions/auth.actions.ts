import { createAction, props } from '@ngrx/store';
import { ICredentials } from 'src/app/models/ICredentials';
import { IUser } from 'src/app/models/IUser';

export const login = createAction(
  '[Auth] Login',
  props<ICredentials>()
);

export const loginSuccess = createAction(
  '[Auth] Login success',
  props<{ token: string, user: IUser }>()
);

export const loginFaliure = createAction(
  '[Auth] Login faliure',
  props<{ errorMessage: string }>()
);

export const register = createAction(
  '[Auth] Register',
  props<ICredentials>()
);

export const registrationSuccess = createAction(
  '[Auth] Register success',
  props<IUser>()
);

export const registrationFaliure = createAction(
  '[Auth] Register faliure',
  props<{ errorMessage: string }>()
);
