import { createAction, props } from '@ngrx/store';
import { IDocument } from 'src/app/models/IDocument';

export const fetchDocuments = createAction(
  '[Documents] Fetch documents',
);
export const fetchSuccess = createAction(
  '[Documents] Fetch documents success',
  props<{ documents: IDocument[] }>()
);
export const fetchFaliure = createAction(
  '[Documents] Fetch documents faliure',
  props<{ errorMessage: string }>()
);

export const uploadDocuments = createAction(
  '[Documents] Upload documents',
  props<{ documents: string[] }>()
);
export const uploadSuccess = createAction(
  '[Documents] Upload documents success',
  props<{ documents: IDocument[] }>()
);
export const uploadFaliure = createAction(
  '[Documents] Upload documents faliure',
  props<{ errorMessage: string }>()
);

export const deleteDocuments = createAction(
  '[Documents] Delete documents',
  props<{ documentIds: number[] }>()
);

export const addDocumentsToCategory = createAction(
  '[Documents] Add documents to category',
  props<{ categoryName: string }>()
);
export const addDocumentsToCategorySuccess = createAction(
  '[Documents] Add documents to category success',
  props<{ documents: IDocument[] }>()
);
export const addDocumentsToCategoryFaliure = createAction(
  '[Documents] Add documents to category faliure',
  props<{ errorMessage: string }>()
);

export const toggleDocumentSelection = createAction(
  '[Documents] Toggle document selection',
  props<{ documentId: number }>()
);
export const toggleCategorySelection = createAction(
  '[Documents] Toggle document category selection',
  props<{ categoryName: string }>()
);
