import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, catchError, mergeMap, withLatestFrom } from 'rxjs/operators';
import { DocumentsService } from 'src/app/services/documents.service';
import { fetchDocuments, fetchSuccess, fetchFaliure, uploadDocuments,
  uploadSuccess, uploadFaliure, addDocumentsToCategory,
  addDocumentsToCategorySuccess, addDocumentsToCategoryFaliure } from '../actions/document.actions';
import { Store } from '@ngrx/store';
import * as _ from 'lodash';

@Injectable()
export class DocumentEffects {

  constructor(
    private actions$: Actions,
    private store$: Store,
    private documentService: DocumentsService,
  ) { }

  fetch$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(fetchDocuments),
      mergeMap(
        () => this.documentService.getDocuments().pipe(
          map((documents) => fetchSuccess({ documents })),
          catchError(({ statusText }) => of(fetchFaliure({ errorMessage: statusText })))
        )
      )
    );
  });

  upload$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(uploadDocuments),
      mergeMap(
        ({ documents }) => this.documentService.createDocuments(documents).pipe(
          map((doc) => uploadSuccess({ documents: doc })),
          catchError(({ statusText }) => of(uploadFaliure({ errorMessage: statusText })))
        )
      )
    );
  });

  categorize$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(addDocumentsToCategory),
      withLatestFrom(this.store$),
      mergeMap(
        ([{ categoryName }, appState ]) => {
          return this.documentService.categorizeDocuments(
            { documentIds: _.get(appState, 'documents.selectedDocumentIds', []), categoryName }
          ).pipe(
            map((doc) => addDocumentsToCategorySuccess({ documents: doc })),
            catchError(({ statusText }) => of(addDocumentsToCategoryFaliure({ errorMessage: statusText })))
          );
        }
      )
    );
  });
}
