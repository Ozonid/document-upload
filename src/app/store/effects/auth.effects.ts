import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { AuthService } from '../../services/auth.service';
import { of } from 'rxjs';
import { map, catchError, mergeMap } from 'rxjs/operators';
import { login, loginSuccess, loginFaliure, register, registrationFaliure, registrationSuccess } from '../actions/auth.actions';
import { Router } from '@angular/router';

@Injectable()
export class AuthEffects {

  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private router: Router,
  ) { }

  login$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(login),
      mergeMap(
        (credentials) => this.authService.login(credentials).pipe(
          map((data) => {
            localStorage.setItem('token', data.token);
            this.router.navigateByUrl('/documents');
            return loginSuccess(data);
          }),
          catchError(({ statusText }) => {
            return of(loginFaliure({ errorMessage: statusText }));
          })
        )
      )
    );
  });

  register$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(register),
      mergeMap(
        (credentials) => this.authService.register(credentials).pipe(
          map((user) => {
            this.router.navigateByUrl('/login');
            return registrationSuccess(user);
          }),
          catchError(({ statusText }) => {
            return of(registrationFaliure({ errorMessage: statusText }));
          })
        )
      )
    );
  });
}
