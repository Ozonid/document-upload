import { Action, createReducer, on } from '@ngrx/store';
import { IDocument } from 'src/app/models/IDocument';
import { fetchDocuments, fetchSuccess, fetchFaliure, uploadDocuments,
  uploadSuccess, uploadFaliure, addDocumentsToCategorySuccess,
  addDocumentsToCategoryFaliure, addDocumentsToCategory, toggleDocumentSelection,
  toggleCategorySelection } from '../actions/document.actions';

export interface State {
  isLoading: boolean;
  documents: IDocument[];
  selectedDocumentIds: number[];
  errorMessage: string | null;
  selectedCategory: string;
}

export const initialState: State = {
  isLoading: false,
  documents: [],
  selectedDocumentIds: [],
  selectedCategory: 'null',
  errorMessage: null
};

const reducer = createReducer(
  initialState,
  on(fetchDocuments, uploadDocuments, addDocumentsToCategory, (state) => {
    return {
      ...state,
      isLoading: true
    };
  }),
  on(fetchSuccess, uploadSuccess, (state, { documents }) => {
    return {
      ...state,
      isLoading: false,
      documents,
      errorMessage: null
    };
  }),
  on(addDocumentsToCategorySuccess, (state, { documents }) => {
    return {
      ...state,
      isLoading: false,
      documents,
      errorMessage: null,
      selectedDocumentIds: []
    };
  }),
  on(fetchFaliure, uploadFaliure, addDocumentsToCategoryFaliure, (state, { errorMessage }) => {
    return {
      ...state,
      isLoading: false,
      errorMessage
    };
  }),
  on(toggleDocumentSelection, (state, { documentId }) => {
    let selectedDocumentIds: number[];
    if (state.selectedDocumentIds.includes(documentId)) {
      selectedDocumentIds = state.selectedDocumentIds.filter(i => i !== documentId);
    } else {
      selectedDocumentIds = [...state.selectedDocumentIds, documentId];
    }

    return {
      ...state,
      selectedDocumentIds
    };
  }),
  on(toggleCategorySelection, (state, { categoryName }) => {
    return {
      ...state,
      selectedCategory: categoryName
    };
  })
);

export function documentsReducer(state: State | undefined, action: Action) {
  return reducer(state, action);
}
