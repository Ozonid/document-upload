import { Action, createReducer, on } from '@ngrx/store';
import { loginSuccess, loginFaliure, registrationFaliure, register, login } from '../actions/auth.actions';
import { IUser } from 'src/app/models/IUser';

export interface State {
  isLoading: boolean;
  isAuthenticated: boolean;
  token: string | null;
  errorMessage: string | null;
  user: IUser | null;
}

const initialToken = localStorage.getItem('token');
export const initialState: State = {
  isLoading: false,
  isAuthenticated: !!initialToken,
  token: initialToken,
  errorMessage: null,
  user: null
};

const reducer = createReducer(
  initialState,
  on(login, register, (state) => {
    return {
      ...state,
      isLoading: true,
      errorMessage: null
    };
  }),
  on(loginSuccess, (state, { token, user }) => {
    localStorage.setItem('token', token);
    return {
      ...state,
      isLoading: false,
      isAuthenticated: true,
      errorMessage: null,
      token,
      user
    };
  }),
  on(loginFaliure, registrationFaliure, (state, { errorMessage }) => {
    return {
      ...state,
      isLoading: false,
      errorMessage
    };
  })
);

export function authReducer(state: State | undefined, action: Action) {
  return reducer(state, action);
}
