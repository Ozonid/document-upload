export interface IDocument {
  id: number;
  name: string;
  category: string;
}
