import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { IUser } from './models/IUser';
import * as _ from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import { ICredentials } from './models/ICredentials';
import { IDocument } from './models/IDocument';

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const { url, method, body } = request;

    switch (true) {
      case url.endsWith('/login') && method === 'POST':
        return login();
      case url.endsWith('/register') && method === 'POST':
        return register();
      case url.endsWith('/documents') && method === 'GET':
        return fetchDocuments();
      case url.endsWith('/documents') && method === 'POST':
        return createDocuments();
      case url.endsWith('/documents/category') && method === 'POST':
        return categorizeDocuments();
      default:
        return next.handle(request);
    }

    function getUsers(): IUser[] {
      return JSON.parse(localStorage.getItem('users')) || [];
    }

    function login() {
      const credentials = body as ICredentials;
      const users = getUsers();

      const user = _.find(users, { email: credentials.email, password: credentials.password });

      if (user) {
        const response = {
          user,
          token: uuidv4()
        };
        return of(new HttpResponse({ status: 200, body: response }));
      }

      return throwError(new HttpErrorResponse({ status: 401, statusText: 'Invalid username or password' }));
    }

    function register() {
      const newUser = body as ICredentials;
      const users = getUsers();

      const userExists = _.find(users, { email: newUser.email });

      if (userExists) {
        return throwError(new HttpErrorResponse({ status: 409, statusText: 'Email already exists' }));
      }

      localStorage.setItem('users', JSON.stringify([...users, {
        id: users.length,
        ...newUser
      }]));

      return of(new HttpResponse({status: 200}));
    }

    function getDocuments() {
      return JSON.parse(localStorage.getItem('documents')) || [];
    }
    function storeDocuments(documents: IDocument[]) {
      localStorage.setItem('documents', JSON.stringify(documents));
    }

    function fetchDocuments() {
      const documents = getDocuments();
      return of(new HttpResponse({ status: 200, body: documents }));
    }

    function createDocuments() {
      const existingDocuments = getDocuments();
      const newDocuments: IDocument[] = body.map((doc: string, idx: number) => ({
        id: existingDocuments.length + idx,
        name: doc,
        category: null
      }));

      const documents = [...existingDocuments, ...newDocuments];

      storeDocuments(documents);
      return of(new HttpResponse({ status: 200, body: documents }));
    }

    function categorizeDocuments() {
      const documents = getDocuments();
      const updatedDoc = documents.map((doc: IDocument) => {
        if (body.documentIds.includes(doc.id)) {
          doc.category = body.categoryName;
        }

        return doc;
      });

      storeDocuments(updatedDoc);
      return of(new HttpResponse({ status: 200, body: updatedDoc }));
    }
  }
}
