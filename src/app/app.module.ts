import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { EffectsModule } from '@ngrx/effects';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './public/login/login.component';
import { RegisterComponent } from './public/register/register.component';
import { StoreModule } from '@ngrx/store';
import { authReducer } from './store/reducers/auth.reducer';

import { AuthEffects } from './store/effects/auth.effects';
import { FakeBackendInterceptor } from './fakeBackend';
import { DocumentsComponent } from './private/documents/documents.component';
import { documentsReducer } from './store/reducers/documents.reducer';
import { DocumentEffects } from './store/effects/document.effects';
import { DocumentListComponent } from './private/documents/document-list/document-list.component';
import { ControlPanelComponent } from './private/documents/control-panel/control-panel.component';
import { DocumentUploadComponent } from './private/documents/document-upload/document-upload.component';

const store = {
  auth: authReducer,
  documents: documentsReducer
};

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    DocumentsComponent,
    DocumentUploadComponent,
    DocumentListComponent,
    ControlPanelComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    StoreModule.forRoot(store),
    EffectsModule.forRoot([AuthEffects, DocumentEffects])
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: FakeBackendInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
